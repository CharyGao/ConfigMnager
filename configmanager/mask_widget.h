#ifndef MASK_WIDGET_H
#define MASK_WIDGET_H

#include <QWidget>
#include "Qss.h"

namespace Ui {
class mask_widget;
}

class mask_widget : public QssMaskWidget
{
    Q_OBJECT

public:
    explicit mask_widget(QWidget *parent = nullptr);
    ~mask_widget();

private:
    Ui::mask_widget *ui;
};

#endif // MASK_WIDGET_H
