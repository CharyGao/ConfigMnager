#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>
#include "pattern/signleton.hpp"
#include "Qss.h"
#include <QStandardItemModel>
#include <QItemSelectionModel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
typedef enum{
    TYPE_NONE = 0,
    TYPE_JSON,
    TYPE_YAML,
    TYPE_INI,
    TYPE_XML
}TYPE_CONFIG;

class MainWindow : public QssMainWindow
{
    Q_OBJECT
public slots:
    void slot_treeView_pressed(QModelIndex);
    void currentChanged(const QModelIndex &current, const QModelIndex &previous);

public:
    MainWindow(QWidget *parent = nullptr,float scale = 1);
    ~MainWindow();

private slots:
    void on_btnouput_clicked();

    void on_btnimoirt_clicked();

    void on_pushButton_clicked();

    void on_config_value_changed(QStandardItem *);


private:
    Ui::MainWindow *ui;
    QStandardItemModel* mModel;
    bool mInit;
    QStandardItemModel* mEditModel;
    TYPE_CONFIG mType;
    QString mSrcPath;
};
#endif // MAINWINDOW_H
