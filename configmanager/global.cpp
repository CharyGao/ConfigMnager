#include "global.h"


CIConMnagager::CIConMnagager()
{
    gPublicIconMap.insert(pair<QString,QIcon>(QString("tree_key"),QIcon(":/qss/icon/key.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("tree_value_int"),QIcon(":/qss/icon/int.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("tree_value_object"),QIcon(":/qss/icon/objects.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("tree_value_array"),QIcon(":/qss/icon/array.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("tree_value_string"),QIcon(":/qss/icon/string.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("missing"),QIcon(":/qss/icon/missing.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("int"),QIcon(":/qss/icon/int.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("bolean"),QIcon(":/qss/icon/bolean.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("missing"),QIcon(":/qss/icon/missing.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("object"),QIcon(":/qss/icon/objects.svg")));
    gPublicIconMap.insert(pair<QString,QIcon>(QString("array"),QIcon(":/qss/icon/array.svg")));

}

QIcon CIConMnagager::GetIcon(QString key)
{
    if(this->gPublicIconMap.find(key) != this->gPublicIconMap.end()){
        return gPublicIconMap[key];
    }else{
        return gPublicIconMap["missing"];
    }
}



