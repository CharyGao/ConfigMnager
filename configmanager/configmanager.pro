QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(D:\\project\\c++qt\\qsswraper\\qsswraper.pri)
INCLUDEPATH += third/include/generallib
INCLUDEPATH += third/include/
INCLUDEPATH += third/include/libxml2

CONFIG += C++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    config.cpp \
    fileparser.cpp \
    global.cpp \
    main.cpp \
    mainwindow.cpp \
    mask_widget.cpp \
    progressdialog.cpp

HEADERS += \
    config.h \
    fileparser.h \
    global.h \
    mainwindow.h \
    mask_widget.h \
    progressdialog.h

FORMS += \
    mainwindow.ui \
    mask_widget.ui \
    progressdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    res/dir.svg \
    res/dir_open.svg

message("message is"  $$PWD)
unix|win32: LIBS += -L $$PWD\third\\lib  -lyaml-cpp -lxml2 -lws2_32 -lz -liconv
LIBS += D:/project/c++/generallib/build/libgeneral.a -static-libstdc++ -static-libgcc
LIBS += -LD:/project/qt/ConfigMnager/configmanager/third/lib -lyaml-cpp
RESOURCES +=
