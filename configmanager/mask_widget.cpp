#include "mask_widget.h"
#include "ui_mask_widget.h"

mask_widget::mask_widget(QWidget *parent) :
    QssMaskWidget(parent),
    ui(new Ui::mask_widget)
{
    ui->setupUi(this);
}

mask_widget::~mask_widget()
{
    delete ui;
}
